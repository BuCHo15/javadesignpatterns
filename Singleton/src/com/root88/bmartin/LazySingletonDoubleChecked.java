package com.root88.bmartin;

/**
 * Created by bmartin on 3/29/15.
 */
public class LazySingletonDoubleChecked {

    private static volatile LazySingletonDoubleChecked INSTANCE = null;

    private LazySingletonDoubleChecked() {

    }

    public static LazySingletonDoubleChecked getINSTANCE() {
        // local variable increases performance by 25 percent
        // Joshua Bloch "Effective Java, Second Edition", p. 283-284
        LazySingletonDoubleChecked result = INSTANCE;
        if (null == result) {
            synchronized (LazySingletonDoubleChecked.class) {
                result = INSTANCE;
                if (null == result) {
                    INSTANCE = result = new LazySingletonDoubleChecked();
                }
            }
        }
        return result;
    }


}
