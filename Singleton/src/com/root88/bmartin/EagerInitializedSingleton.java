package com.root88.bmartin;

/**
 * Created by bmartin on 3/29/15.
 */
public class EagerInitializedSingleton {

    private static EagerInitializedSingleton INSTANCE = new EagerInitializedSingleton();

    private EagerInitializedSingleton() {
    }

    public static EagerInitializedSingleton getINSTANCE() {
        return INSTANCE;
    }
}


