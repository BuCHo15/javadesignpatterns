package com.root88.bmartin;

/**
 * Created by bmartin on 3/29/15.
 * Thread-safe Singleton class.
 * The instance is lazily initialized and therefore needs a synchronization mechanism.
 */
public class LazySingleton {

    private static LazySingleton INSTANCE = null;

    private LazySingleton() {

    }

    public synchronized static LazySingleton getINSTANCE() {
        if (null == INSTANCE) {
            INSTANCE = new LazySingleton();
        }
        return INSTANCE;
    }

}
