package com.root88.bmartin;

public class Main {

    public static void main(String[] args) {

        LazySingleton l1 = LazySingleton.getINSTANCE();
        LazySingleton l2 = LazySingleton.getINSTANCE();

        System.out.println("LazySingleton 1 = " + l1);
        System.out.println("LazySingleton 2 = " + l2);
        System.out.println("Is working ? " + (l1 == l2));


        LazySingletonDoubleChecked l3 = LazySingletonDoubleChecked.getINSTANCE();
        LazySingletonDoubleChecked l4 = LazySingletonDoubleChecked.getINSTANCE();

        System.out.println("LazySingletonDoubleChecked 1 = " + l3);
        System.out.println("LazySingletonDoubleChecked 2 = " + l4);
        System.out.println("Is working ? " + (l3 == l4));


        EagerInitializedSingleton l5 = EagerInitializedSingleton.getINSTANCE();
        EagerInitializedSingleton l6 = EagerInitializedSingleton.getINSTANCE();

        System.out.println("EagerInitializedSingleton 1 = " + l5);
        System.out.println("EagerInitializedSingleton 2 = " + l6);
        System.out.println("Is working ? " + (l5 == l6));

    }
}
