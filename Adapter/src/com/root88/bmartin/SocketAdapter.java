package com.root88.bmartin;

/**
 * Created by bmartin on 3/29/15.
 */
public interface SocketAdapter {

    public Volt get120Volt();

    public Volt get12Volt();

    public Volt get3Volt();
    
}
