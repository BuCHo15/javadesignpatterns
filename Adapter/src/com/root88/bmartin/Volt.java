package com.root88.bmartin;

/**
 * Created by bmartin on 3/29/15.
 */
public class Volt {

    private int volts;

    public Volt(int v) {
        this.volts = v;
    }

    public int getVolts() {
        return volts;
    }

    public void setVolts(int volts) {
        this.volts = volts;
    }


}
